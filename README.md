### nanopass 
A quasi-deterministic password management algorithm in ~300LoC

---
**Differences**

Nanopass has a few key advantages over other determinstic password managers. 

- **Flexible password parameters** - nanopass can generate passwords with specified lengths and various special character restrictions.

- **Mutable master passwords** - master passwords can be changed while keeping the same generated passwords, yielding a new vault.

- **Password versioning** - a single generated password can be updated without a new master password or changing other generated passwords.

Determinstic password generation also has a few differences from a typical manager.

- **Trust** - nanopass never stores your passwords, encrypted or otherwise.

- **Portability** - passwords can be generated completely offline, with only a small optional configuration file required. 

---
**Config spec**

Configuration can be imported a json file with the following format:

```json
[
    {
        "name": "<name>",
        "length": <length>, // password length up to 42
        "version": <version>,
        "special": "<chars>" // excluded special characters
    }                
]
```

