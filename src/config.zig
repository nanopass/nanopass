const std = @import("std");
const hash = std.hash;
const json = std.json;
const fmt = std.fmt;
const meta = std.meta;
const ALLOC = std.heap.page_allocator;

const SITE_SIZE = @bitSizeOf(SiteBits) / 8;
pub const MAX_SIZE = 3000;

pub const SPECIALS_C = @import("pass.zig").CHARSET[3];
const SPECIALS_I = gen_specials();
fn gen_specials() [256]u3 {
    var indexes: [256]u3 = undefined;
    inline for (0..8) |i| {
        indexes[SPECIALS_C[i]] = i;
    }
    return indexes;
}

pub const Site = struct {
    name: []const u8,
    length: u8,
    version: u8,
    special: []const u8,
};

pub const SiteBits = packed struct {
    name: u64, // hashed site
    length: u8, // password length
    version: u8, // iteration for changes
    special: u8, // disallowed special characters as bitmask (atleast one in password if enabled)
};

// packs site into packed struct
pub fn pack_site(site: Site) SiteBits {
    const name = hash.XxHash64.hash(0, site.name);
    var special: u8 = 0;
    for (site.special) |c| {
        special |= (@as(u8, 1) << SPECIALS_I[c]);
    }
    return SiteBits{ .name = name, .length = site.length, .version = site.version, .special = special };
}

pub fn get_site(name: []const u8, sites: []const SiteBits) !SiteBits {
    const n_hash = hash.XxHash64.hash(0, name);
    for (sites) |site| {
        if (site.name == n_hash) {
            return site;
        }
    }
    return error.SiteNotFound;
}

// parses json config to byte packed representation
pub fn json_to_bits(input: []const u8, vault: [64]u8) ![]const u8 {
    const raw = try json.parseFromSlice([]Site, ALLOC, input, .{});
    defer raw.deinit();

    var out = std.ArrayList(u8).init(ALLOC);

    // append vault as bytes
    var buffer: [64]u8 = undefined;
    try out.appendSlice(try fmt.hexToBytes(&buffer, &vault));

    // append each site as bytes
    const sites = raw.value;
    for (0..sites.len) |i| {
        const site: [SITE_SIZE]u8 = @bitCast(pack_site(sites[i]));
        try out.appendSlice(&site);
    }

    return try out.toOwnedSlice();
}

pub fn conf_to_bits(vault: [32]u8, sites: []const SiteBits) ![]const u8 {
    var out = std.ArrayList(u8).init(ALLOC);
    try out.appendSlice(&vault);

    for (0..sites.len) |i| {
        const site: [SITE_SIZE]u8 = @bitCast(sites[i]);
        try out.appendSlice(&site);
    }

    return try out.toOwnedSlice();
}

// deseralizies vault for internal use
pub fn bits_to_conf(bits: []const u8) !meta.Tuple(&.{ [32]u8, []const SiteBits }) {
    const vault = bits[0..32];
    const site_bits = bits[32..];
    var sites = std.ArrayList(SiteBits).init(ALLOC);

    // loop through each SITE_SIZE chunk of bytes and convert
    var i: usize = 0;
    while (i < site_bits.len) : (i += SITE_SIZE) {
        const site: *const [SITE_SIZE]u8 = @ptrCast(site_bits[i .. i + SITE_SIZE]);
        try sites.append(@bitCast(site.*));
    }

    return .{ vault.*, try sites.toOwnedSlice() };
}
