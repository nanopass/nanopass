const std = @import("std");
const pass = @import("pass.zig");
const vault = @import("vault.zig");
const config = @import("config.zig");
pub const Site = config.Site;

const ALLOC = std.heap.page_allocator;

pub fn comp_json(json: []const u8, hex: [64]u8) ![]const u8 {
    return try config.json_to_bits(json, hex);
}

pub fn get_pass(bits: []const u8, name: []const u8, master: []const u8) ![]const u8 {
    const conf = try config.bits_to_conf(bits);
    const key = try vault.get_key(conf[0], master);
    const site = try config.get_site(name, conf[1]);
    return try pass.get_pass(site, key);
}

pub fn rn_pass(bits: []const u8, old: []const u8, new: []const u8) ![]const u8 {
    const conf = try config.bits_to_conf(bits);
    const up = try vault.rn_pass(conf[0], old, new);
    return try config.conf_to_bits(up, conf[1]);
}

pub fn add_site(bits: []const u8, site: config.Site) ![]const u8 {
    const conf = try config.bits_to_conf(bits);
    var sites = std.ArrayList(config.SiteBits).init(ALLOC);
    try sites.appendSlice(conf[1]);
    try sites.append(config.pack_site(site));
    return config.conf_to_bits(conf[0], try sites.toOwnedSlice());
}

pub fn rm_site(bits: []const u8, name: []const u8) ![]const u8 {
    const conf = try config.bits_to_conf(bits);
    var sites = std.ArrayList(config.SiteBits).init(ALLOC);
    try sites.appendSlice(conf[1]);
    const site = try config.get_site(name, conf[1]);
    for (0..conf[1].len) |i| {
        if (conf[1][i].name == site.name) {
            _ = sites.swapRemove(i);
            break;
        }
    }
    return config.conf_to_bits(conf[0], try sites.toOwnedSlice());
}
