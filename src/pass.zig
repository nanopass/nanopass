const std = @import("std");
const blake3 = std.crypto.hash.Blake3;
const big = std.math.big;

const SiteBits = @import("config.zig").SiteBits;
const SiteRaw = packed struct { name: u64, version: u8 };

pub const CHARSET = .{
    "abcdefghijklmnopqrstuvwxyz",
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
    "0123456789",
    "!@#$%^&*",
};
const L_COUNT = 8; // limb count for bigints
const MAX_SIZE = 42; // limit length of password to avoid running out of entropy

// returns allowed special chars
fn get_specials(special: u8) !std.BoundedArray(u8, 8) {
    var allowed = try std.BoundedArray(u8, 8).init(0);
    for (0..8) |i| {
        // if allowed add to array
        const b = (special >> @intCast(i)) & 1;
        if (b == 0) {
            try allowed.append(CHARSET[3][i]);
        }
    }
    // replace specials
    return allowed;
}

// version + site name + key -> hash
fn get_hash(site: SiteRaw, key: [32]u8) [32]u8 {
    var out: [32]u8 = undefined;
    var blake = blake3.init(.{});
    blake.update(&key);
    blake.update(&@as([@bitSizeOf(SiteRaw) / 8]u8, @bitCast(site)));
    blake.final(&out);
    return out;
}

// generates password from sitebits and vault
// requires at least one of each type of character
pub fn get_pass(site: SiteBits, key: [32]u8) ![]const u8 {

    // setup charset
    const specials = try get_specials(site.special);
    const charset_len = switch (site.special) {
        0xff => CHARSET.len - 1,
        else => CHARSET.len,
    };

    if (site.length < charset_len or site.length > MAX_SIZE) {
        return error.InvalidSiteLength;
    }

    // setup bigint
    var limbs: [L_COUNT]big.Limb = undefined;
    var raw = big.int.Mutable.init(&limbs, 0);
    const hash = get_hash(SiteRaw{ .name = site.name, .version = site.version }, key);
    raw.readTwosComplement(&hash, 256, std.builtin.Endian.little, std.builtin.Signedness.unsigned);

    // allocate output
    var pass = try std.BoundedArray(u8, MAX_SIZE).init(0);

    // one of each type to start
    for (0..charset_len) |i| {
        const set = switch (i) {
            0 => CHARSET[0],
            1 => CHARSET[1],
            2 => CHARSET[2],
            else => specials.constSlice(),
        };
        const r = try div_mod(&raw, set.len);
        try pass.append(set[r]);
    }

    // fill rest with random
    const flattened = CHARSET[0] ++ CHARSET[1] ++ CHARSET[2];
    for (0..(site.length - charset_len)) |_| {
        const r = try div_mod(&raw, flattened.len + specials.len);
        if (r < flattened.len) {
            try pass.append(flattened[r]);
        } else {
            try pass.append(specials.constSlice()[r - flattened.len]);
        }
    }

    // permute first of each type to rest
    for (0..charset_len) |i| {
        const r = try div_mod(&raw, site.length - charset_len);

        const t = pass.get(r + charset_len);
        pass.set(r + charset_len, pass.get(i));
        pass.set(i, t);
    }

    return try std.heap.page_allocator.dupe(u8, pass.constSlice());
}

// r = divmod(*q, d)
inline fn div_mod(q: *big.int.Mutable, d: usize) !usize {
    var limbs_t: [L_COUNT]big.Limb = undefined;
    var temp = big.int.Mutable.init(&limbs_t, d);
    var limbs_r: [L_COUNT]big.Limb = undefined;
    var rem = big.int.Mutable.init(&limbs_r, 0);
    var limbs_d: [L_COUNT]big.Limb = undefined;

    q.divTrunc(&rem, q.toConst(), temp.toConst(), &limbs_d);
    return rem.toConst().to(usize);
}
