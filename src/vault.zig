const blake3 = @import("std").crypto.hash.Blake3;
const CONTEXT = "nanopass_kdf";

// xors byte arrays
fn xor_bytes(hash: [32]u8, vault: [32]u8) [32]u8 {
    var out: [32]u8 = undefined;
    for (0..32) |i| {
        out[i] = hash[i] ^ vault[i];
    }
    return out;
}

// derives hash from password
fn hash_pass(pass: []const u8) ![32]u8 {
    if (pass.len == 0) {
        return error.InvalidPassword;
    }
    var out: [32]u8 = undefined;
    var blake = blake3.initKdf(CONTEXT, .{});
    blake.update(pass);
    blake.final(&out);
    return out;
}

// (pass -> hash) ^ vault -> key
pub fn get_key(vault: [32]u8, pass: []const u8) ![32]u8 {
    const hash = try hash_pass(pass);
    return xor_bytes(hash, vault);
}

// returns new vault with same key for different password
pub fn rn_pass(vault: [32]u8, old: []const u8, new: []const u8) ![32]u8 {
    const key = try get_key(vault, old);
    return try get_key(key, new);
}
